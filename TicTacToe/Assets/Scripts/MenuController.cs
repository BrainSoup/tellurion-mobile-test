﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {

    [SerializeField]
    private Transform ScoresMenu;

    public void OnStartClick()
    {

    }

    public void OnLoadClick()
    {

    }

    public void OnScoresClick()
    {
        if (ScoresMenu == null)
        {
            Debug.LogError(this + ": ScoresMenu is not initialised.");
            return;
        }
        Instantiate(ScoresMenu, new Vector3(0, 0, 0), Quaternion.identity);
    }

    public void OnQuitClick()
    {
        Application.Quit();
    }

}
