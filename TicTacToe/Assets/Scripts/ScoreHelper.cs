﻿using UnityEngine;

public static class ScoreHelper {

    private const string TotalGames = "TotalWins";
    private const string TotalWins = "TotalGames";

    public static int GamesTotal {
        get { return PlayerPrefs.GetInt(TotalGames); }
        private set { PlayerPrefs.SetInt(TotalGames, value); } }

    public static int Wins
    {
        get { return PlayerPrefs.GetInt(TotalWins); }
        private set { PlayerPrefs.SetInt(TotalWins, value); }
    }

    public static int Loses
    {
        get { return GamesTotal - Wins; }
    }

    public static void ProcessGameScores(bool win)
    {
        GamesTotal++;

        if (win)
            Wins++;
    }

    public static int GetTotalGames()
    {
        return PlayerPrefs.GetInt(TotalGames);
    }
}
