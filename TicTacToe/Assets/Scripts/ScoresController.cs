﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoresController : MonoBehaviour {

    [SerializeField]
    private Text TotalGames;
    [SerializeField]
    private Text TotalWins;
    [SerializeField]
    private Text TotalLoses;

    void Start () {
        TotalGames.text = ScoreHelper.GamesTotal.ToString();
        TotalWins.text = ScoreHelper.Wins.ToString();
        TotalLoses.text = ScoreHelper.Loses.ToString();
    }

    public void CloseMenu()
    {
        Destroy(this.transform.parent.gameObject);
    }

}
